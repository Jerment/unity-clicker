using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class ResourceVisual : MonoBehaviour
{
    public GameResource GameResource;
    [SerializeField] private Text resourceText;
    [SerializeField] private ResourceBank resourceBank;

    private void Start()
    {
        resourceBank = Game.Intance.ResourceBank;
        UpdateText(resourceText);
    }
    public void UpdateText(Text resourceText)
    {
        resourceText.text = resourceBank.GetResource(GameResource).ToString();
    }
}
