using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ResourceBank : MonoBehaviour
{
    private Dictionary<GameResource, int> resources;

    private void OnEnable()
    {
        resources = new Dictionary<GameResource, int>()
        {
            [GameResource.HUMANS] = Game.Intance.GameConfig.Humans,
            [GameResource.FOOD] = Game.Intance.GameConfig.Food,
            [GameResource.WOOD] = Game.Intance.GameConfig.Wood,
            [GameResource.STONE] = Game.Intance.GameConfig.Stone,
            [GameResource.GOLD] = Game.Intance.GameConfig.Gold
        };
    }
    public int GetResource(GameResource r)
    {
        return resources[r];
    }
    public void ChangeResource(GameResource r, int v)
    {
        if (!resources.ContainsKey(r))
        {
            throw new InvalidDataException();
        }
        resources[r] += v;
    }
}