using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public GameConfig GameConfig;
    public ResourceBank ResourceBank;
    public static Game Intance;

    public void Awake()
    {
        Intance = this;
    }

    private void Start()
    {
        ResourceBank.ChangeResource(GameResource.HUMANS, 10);
        ResourceBank.ChangeResource(GameResource.FOOD, 5);
        ResourceBank.ChangeResource(GameResource.WOOD, 5);
    }
}
