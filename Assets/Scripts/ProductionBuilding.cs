using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ProductionBuilding : MonoBehaviour
{

    [SerializeField] private GameResource gameResource;
    [SerializeField] private Text resourceText;
    [SerializeField] private ResourceBank resourceBank;
    [SerializeField] private Button button;
    [SerializeField] private Slider slider;
    [SerializeField] private int changeValue;
    [SerializeField] private int productionTime;

    private void Awake()
    {
        resourceBank = Game.Intance.ResourceBank;
    }

    public void AddValue()
    {
        resourceBank.ChangeResource(gameResource, 1);
        resourceText.text = resourceBank.GetResource(gameResource).ToString();
        button.interactable = false;
        StartCoroutine(Timer(productionTime, changeValue));
    }

    private IEnumerator Timer(int cooldown, int add)
    {
        slider.maxValue = cooldown;
        slider.value = 0;
        while (slider.value < cooldown)
        {
            slider.value += Time.deltaTime ;
            yield return null;
        }

        button.interactable = true;
    }
}