using UnityEngine;

[CreateAssetMenu(menuName = "Configs", fileName = "GameConfig")]
public class GameConfig : ScriptableObject
{
    [Range(0, 1000)]
    public int Humans;
    [Range(0, 1000)]
    public int Food;
    [Range(0, 1000)]
    public int Wood;
    [Range(0, 1000)]
    public int Gold;
    [Range(0, 1000)]
    public int Stone;
}